# URNAX
Electronic vote project for next municipal elections

## Data
Project use two endpoints for voters and voting places that contains the information of all the voters, and the voting 
places. This data was implement with JSONPlaceholder:
* [Data repository](https://github.com/ilsenluna/urnax-db)
* [Endpoints for data](https://my-json-server.typicode.com/ilsenluna/urnax-db).

## How does it work?

The [solution](https://gitlab.com/joanmdas/urnax/-/blob/main/src/main/scala/Urnax.scala) have the next steps:

1. Load data from endpoints

The application load data from endpoints described above, this data are loaded into objects of list type.
Data contains initial configuration about valid places voting (schools), valid voters (people registered with DNI and 
voting place assigned).

2. Prepare and send messages

After load data of voters and voting places, the application prepare and send messages about the location of the 
assigned voting places to the voters.

3. Prepare list of Votes

In here a map is created with an entry for every voter that shows if a voter has voted or not.

4. Prepare REST server

In this step, a server is configured with a single REST endpoint that is called when a voter votes.

## How to test?

1. Start the application and then open postman.
2. Import the collection present in the [postman folder](./postman/Urnax.postman_collection.json) of this project. 
3. Once imported, you can start sending request using the message printed in the console on the second step as data.
   ![Second step data](./docs/second_step.png)