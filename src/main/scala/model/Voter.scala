package model

//Use of case class in this case since there is only data present with no functionality
case class Voter(id: String, name: String, placeId: String)
