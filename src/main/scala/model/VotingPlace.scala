package model

//Use of case class in this case since there is only data present with no functionality
case class VotingPlace(id: String, name: String, city: String, postalCode: String, address: String, country: String)
