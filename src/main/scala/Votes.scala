import model.Voter

import scala.collection.mutable

//This class has a companion object that hides the private constructor and initializes the structure
class Votes private () {

  //This is a mutable map since the Boolean values will change
  val votes: mutable.Map[String, Boolean] = mutable.Map()

  //This is private since it is only expected to be used by the companion object
  private def init(voterList: List[Voter]): Votes = {
    voterList.foreach(voter => votes += (voter.id -> false))
    this
  }

  def checkVote(voterId: String): Option[Boolean] = votes.get(voterId)

  //We have used {} in this case because the legibility is better in that case
  def vote(voterId: String): Unit = {
    votes(voterId) = true
  }
}
object Votes {
  def apply(voterList: List[Voter]): Votes = {
    new Votes().init(voterList)
  }
}
