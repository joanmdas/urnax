import exceptions.VotingException
import model.{Voter, VotingPlace}

//Object that contains the validation functionality for the votes
object VotingValidation {

  //Validate valid vote
  def checkVoter(listVoters: List[Voter], voterId: String): Option[Voter] = listVoters.find(voter => voter.id.equals(voterId))

  //Validate voting place exists and voter in correct voting place
  def checkVotingPlace(listVotingPlaces: List[VotingPlace], voter: Voter, placeId: String): Option[Boolean] = {
    val validPlace: Option[Boolean] = listVotingPlaces.find(votingPlace => votingPlace.id.equals(placeId)) match {
      case Some(place) => if (place.id.equals(voter.placeId)) Some(true) else Some(false)
      case None => throw VotingException(s"Voting place $placeId not found in voting place list")
    }
    validPlace
  }

  //Validate voter did not vote already
  def checkVoterDidNotVote(votes: Votes, voter: Voter): Option[Boolean] = votes.checkVote(voter.id)
}
