import model.{Voter, VotingPlace}

//This is a companion object that hides the constructor of the message
//It also overrides the toString function in order to convert the message to the desired format based on the constructor data
class Message private (val voter: Voter, val votingPlace: VotingPlace) {
  override def toString: String = s"The voting place for ${voter.name} with DNI ${voter.id} is in the " +
    s"address ${votingPlace.address} of the city ${votingPlace.city} in the country ${votingPlace.country} " +
    s"and has the identifier ${votingPlace.id}."
}
object Message {
  def apply(voter: Voter, votingPlace: VotingPlace):Message = new Message(voter, votingPlace)
}

