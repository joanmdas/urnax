import akka.Done
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import exceptions.VotingException
import http.client.{GetVoters, GetVotingPlaces}
import model.{Voter, VotingPlace}
import spray.json.DefaultJsonProtocol.{StringJsonFormat, jsonFormat2}
import spray.json.RootJsonFormat

import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.concurrent.duration.DurationInt
import scala.io.StdIn
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

object Urnax extends App {

  // 1. Load data from endpoints
  println("***** 1. Load data from endpoints *****")

  val votersListFuture = GetVoters.execute()
  val votingPlacesFuture = GetVotingPlaces.execute()

  val votersList: List[Voter] = Try(Await.result(votersListFuture, 10 seconds)) match {
    case Success(value) => value
    case Failure(exception) => throw VotingException(s"Unable to get voters list due to ${exception.getMessage}")
  }

  val votingPlaces: List[VotingPlace] = Try(Await.result(votingPlacesFuture, 10 seconds)) match {
    case Success(value) => value
    case Failure(exception) => throw VotingException(s"Unable to get voting places list due to ${exception.getMessage}")
  }

  // 2. Prepare and send messages (println)
  println("***** 2. Prepare and send messages *****")

  votersList.foreach(voter => {
    val votingPlace: VotingPlace = votingPlaces.find(votingPlace => votingPlace.id.equals(voter.placeId)) match {
      case Some(votingPlace) => votingPlace
      case None => throw VotingException(s"Unable to send notification to voter ${voter.name}")
    }

    println(Message(voter, votingPlace).toString)
  })

  // 3. Prepare list of Votes
  println("***** 3. Prepare list of Votes *****")
  val votes: Votes = Votes.apply(votersList)

  // 4. Prepare REST server
  println("***** 4. Prepare REST server *****")

  //This is an exception handler for the REST endpoint created
  val exceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case votingException: VotingException => complete(votingException.message)
      case ex: Exception => complete(s"Unexpected exception: ${ex.getMessage}")
    }

  implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "Request")
  implicit val executionContext: ExecutionContextExecutor = system.executionContext

  //Case class containing the expected request data
  case class VoteRequest(idVoter: String, idVotingPlace: String)

  implicit val voteRequestFormat: RootJsonFormat[VoteRequest] = jsonFormat2(VoteRequest)

  //This function calls the execution of a vote
  def saveVote(idVoter:String, idVotingPlace:String): Future[Done] = {
    Vote.execute(idVoter, idVotingPlace, votersList, votingPlaces, votes,
      VotingValidation.checkVoter,
      VotingValidation.checkVotingPlace,
      VotingValidation.checkVoterDidNotVote)
    Future { Done }
  }

  //Definition of the REST endpoint
  val route: Route = handleExceptions(exceptionHandler) {
    concat(
      post {
        path("vote") {
          entity(as[VoteRequest]) { voteRequest =>
            val voted: Future[Done] = saveVote(voteRequest.idVoter, voteRequest.idVotingPlace)
            onSuccess(voted) { _ => // use of _ since we don't care about the result, only that it is successful
              complete("vote send")
            }
          }
        }
      }
    )
  }

  //Start server
  val bindingFuture = Http().newServerAt("localhost", 8080).bind(route)
  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine()
  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
}
