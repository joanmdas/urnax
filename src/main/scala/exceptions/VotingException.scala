package exceptions

//This is an example of a hierarchy since it extends from Exception
case class VotingException(message: String) extends Exception(message)
