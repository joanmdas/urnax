package http.client

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import model.VotingPlace
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

import scala.concurrent.{ExecutionContextExecutor, Future}

//Get voting places from external resource using showing the use of Future and http requests
object GetVotingPlaces {
  def execute(): Future[List[VotingPlace]] = {
    implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "Request")
    implicit val executionContext: ExecutionContextExecutor = system.executionContext
    implicit val votingPlace: RootJsonFormat[VotingPlace] = jsonFormat6(VotingPlace)

    val responseFuture: Future[List[VotingPlace]] = Http()
      .singleRequest(HttpRequest(uri = "https://my-json-server.typicode.com/ilsenluna/urnax-db/votingPlace"))
      .flatMap(response => {
        Unmarshal(response).to[List[VotingPlace]]
      })
    responseFuture
  }
}
