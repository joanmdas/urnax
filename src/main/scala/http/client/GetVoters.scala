package http.client

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import model.Voter
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

import scala.concurrent.{ExecutionContextExecutor, Future}

//Get voters from external resource using showing the use of Future and http requests
object GetVoters {
  def execute(): Future[List[Voter]] = {
    implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "Request")
    implicit val executionContext: ExecutionContextExecutor = system.executionContext
    implicit val voter: RootJsonFormat[Voter] = jsonFormat3(Voter)

    val responseFuture: Future[List[Voter]] = Http()
      .singleRequest(HttpRequest(uri = "https://my-json-server.typicode.com/ilsenluna/urnax-db/voter"))
      .flatMap(response => {
        Unmarshal(response).to[List[Voter]]
      })
    responseFuture
  }
}
