import exceptions.VotingException
import model.{Voter, VotingPlace}

object Vote {

  //This is a High Order Function that contains all the necessary data and functionality to execute the vote.
  def execute(voterId: String,
              placeId: String,
              votersList: List[Voter],
              votingPlaces: List[VotingPlace],
              votes: Votes,
              validVoter: (List[Voter], String) => Option[Voter],
              validPlace: (List[VotingPlace], Voter, String) => Option[Boolean],
              voterNotVoted: (Votes, Voter) => Option[Boolean]): Unit = {

    //Examples of pattern matching usage when dealing with Option
    val voter: Voter = validVoter.apply(votersList, voterId) match {
      case Some(value) => value
      case None => throw VotingException(s"Voter $voterId not found")
    }

    val validLocation: Boolean = validPlace.apply(votingPlaces, voter, placeId) match {
      case Some(value) => value
      case None => throw VotingException(s"Voting place for voter ${voter.name} not found")
    }

    if (!validLocation) throw VotingException(s"Invalid voting place for voter ${voter.name}")

    val voted: Boolean = voterNotVoted.apply(votes, voter) match {
      case Some(value) => value
      case None => throw VotingException(s"Voter ${voter.name} not found in votes list")
    }

    if (voted) throw VotingException(s"Voter ${voter.name} already voted")
    else votes.vote(voterId)

    println(s"Voter ${voter.name} has voted")
  }
}
